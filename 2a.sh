#!/bin/bash

#eusq receba um número como argumento de linha de comando e imprima a soma de todos os números de 1 até este número.

n=$1

echo $(( (n*(n-1))/2))

