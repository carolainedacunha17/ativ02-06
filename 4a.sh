#!/bin/bash

#eusq receba vários nomes de arquivo como argumento de linha de comando e imprima a soma do número de linha s.

soma=0

for a in $@; do
	t=$( wc -l < $a )
	(( soma += t ))

done

echo $soma
