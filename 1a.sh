#!/bin/bash


# euqs que, para cada arquivo dos diretórios /etc e /tmp informe se é um diretório, um arquivo, um link simbólico ou um executável.

for i in $( ls /etc /tmp ); do
	[ -d $i ] && echo $i eh um diretorio && continue
	[ -f $i ] && echo $i eh um arquivo  
	[ -x $i ] && echo $i eita, ainda executavel 
	[ -L $i ] && echo $i eh link 

done

#comando de funciona só dentro do laço: break, onde tem o break ele quebra o laço

#break: para na mesma hora
#continue: vai pra proxima da lista
#no caso, se chegar no primeiro teste e deu certo ele vai parar e voltar pra cima pra fazer um teste com o /tmp

